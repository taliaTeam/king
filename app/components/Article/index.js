/**
 *
 * Article
 *
 */

import React, { memo, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import { Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';
import MyLink from 'components/MyLink';
import ReactSlickResponsive from 'components/ReactSlickResponsive';
import BoxList from 'components/BoxList';
import { ApiContext, LangContext } from 'components/Context/Context';
import messages from './messages';
import './style.scss';

// import styled from 'styled-components';

function Article({ api }) {
  const { ArticleShowAllFunc } = useContext(ApiContext);
  const { locale, dateLocale } = useContext(LangContext);

  // region function
  useEffect(() => {
    if (!api.article) {
      ArticleShowAllFunc();
    }
  }, []);
  // endregion
  if (api.article && api.article.data) {
    return (
      <Grid className={`article ${locale}`}>
        <Typography variant="h5">
          <FormattedMessage {...messages.header} />
          <MyLink href="/article">
            <Fab variant="extended" className="article-more">
              <FormattedMessage {...messages.more} />
            </Fab>
          </MyLink>
        </Typography>
        <Grid>
          <ReactSlickResponsive>
            {[...api.article.data].reverse().map((item, index) => (
              <Grid
                item
                xs={12}
                sm={11}
                key={index.valueOf()}
                className="article-box"
              >
                <MyLink href={`/article/${item.title}`}>
                  <BoxList>
                    <Box className="box-list-date">
                      {dateLocale(item.updated_at)}
                    </Box>
                    <Box className="box-list-title">{item.title}</Box>
                    <Box className="box-list-review">{item.review}</Box>
                    <Box className="box-list-Continue">
                      <FormattedMessage {...messages.Continue} />
                    </Box>
                    <img src={item.preview} alt="" />
                  </BoxList>
                </MyLink>
              </Grid>
            ))}
          </ReactSlickResponsive>
        </Grid>
      </Grid>
    );
  }
  return '';
}

Article.propTypes = {
  api: PropTypes.object.isRequired,
};

export default memo(Article);
