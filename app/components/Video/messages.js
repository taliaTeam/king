/*
 * Video Messages
 *
 * This contains all the text for the Video component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Video';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'ویدیوهای',
  },
});
