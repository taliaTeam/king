/**
 *
 * Video
 *
 */

import React, { memo, useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import SlowMotionVideoIcon from '@material-ui/icons/SlowMotionVideo';
import IconButton from '@material-ui/core/IconButton';
import Fab from '@material-ui/core/Fab';
import Drawer from '@material-ui/core/Drawer';
import { Container } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import messages from './messages';
import BoxList from '../BoxList';
import ReactSlickResponsive from '../ReactSlickResponsive';
import { ApiContext, LangContext } from '../Context/Context';
import './style.scss';

function Video({ api }) {
  const { VideoShowAllFunc } = useContext(ApiContext);
  const [video, setVideo] = useState();
  const { locale } = useContext(LangContext);
  const [state, setState] = useState({
    top: false,
  });

  useEffect(() => {
    if (!api.video) VideoShowAllFunc();
  }, []);
  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };
  const fullList = side => (
    <div
      className="video-player"
      role="presentation"
      onKeyDown={toggleDrawer(side, false)}
    >
      <Fab className="video-play-close" onClick={toggleDrawer(side, false)}>
        <CloseIcon />
      </Fab>
      <Container>
        <Grid container direction="row" justify="center" alignItems="center">
          {/* eslint-disable-next-line jsx-a11y/media-has-caption */}
          <video preload="metadata" src={video} controls autoPlay={false} />
        </Grid>
      </Container>
    </div>
  );
  if (api.video && api.video.data) {
    return (
      <Grid className={`${locale} video`}>
        <Typography variant="h6">
          <FormattedMessage {...messages.header} />
          <Typography variant="inherit" className="video-name-site">
            {api.option.data && api.option.data.name_site.value}
          </Typography>
        </Typography>
        <ReactSlickResponsive>
          {[...api.video.data].reverse().map((item, indexMap) => (
            <Grid
              item
              key={indexMap.valueOf()}
              xs={12}
              sm={11}
              className="video-Box elem-rtl"
            >
              <BoxList>
                <img src={item.preview} alt="" />
                <Grid
                  container
                  direction="row"
                  alignItems="center"
                  justify="center"
                  className="video-icon"
                >
                  <IconButton
                    className="video-icon-button"
                    onClick={e => {
                      setVideo(item.link);
                      toggleDrawer('top', true)(e);
                    }}
                  >
                    <SlowMotionVideoIcon />
                  </IconButton>
                </Grid>
              </BoxList>
              <Drawer
                anchor="top"
                open={state.top}
                onClose={toggleDrawer('top', false)}
              >
                {fullList('top')}
              </Drawer>
            </Grid>
          ))}
        </ReactSlickResponsive>
      </Grid>
    );
  }
  return '';
}

Video.propTypes = {
  api: PropTypes.object.isRequired,
};

export default memo(Video);
