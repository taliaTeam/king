/**
 *
 * RelatedPost
 *
 */

import React, { memo, useContext } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import './style.scss';
import MyLink from '../MyLink';
import { LangContext } from '../Context/Context';

function RelatedPost({ posts, type }) {
  const { locale } = useContext(LangContext);
  return (
    <Grid className={`post-related ${locale}`}>
      <Typography variant="subtitle2" className="post-related-title">
        <FormattedMessage {...messages.header} />
      </Typography>
      {[...posts.related].reverse().map((item, index) => (
        <MyLink href={`/${type}/${item.title}`} className="related-item">
          <Grid
            container
            spacing={2}
            key={index.valueOf()}
            className="post-related-post"
          >
            <Grid item xs={4}>
              <img src={item.preview} alt="" />
            </Grid>
            <Grid item xs={8}>
              <Typography
                variant="subtitle2"
                className="post-related-post-title"
              >
                {item.title}
              </Typography>
              <Typography variant="subtitle2">
                {item.review || item.code}
              </Typography>
            </Grid>
          </Grid>
        </MyLink>
      ))}
    </Grid>
  );
}

RelatedPost.propTypes = {
  posts: PropTypes.object.isRequired,
  type: PropTypes.string,
};
RelatedPost.defaultProps = {
  type: 'project',
};
export default memo(RelatedPost);
