/*
 * RelatedPost Messages
 *
 * This contains all the text for the RelatedPost component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.RelatedPost';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'نوشته های مرتبط',
  },
});
