/**
 *
 * Service
 *
 */

import React, { memo, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
// import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import { Box } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import messages from './messages';
import { ApiContext, LangContext } from '../Context/Context';
import './style.scss';
import MyLink from '../MyLink';
import BoxList from '../BoxList';

function Service({ api }) {
  const { ServiceShowAllFunc } = useContext(ApiContext);
  const { locale, dateLocale } = useContext(LangContext);

  // region function
  useEffect(() => {
    if (!api.service) {
      ServiceShowAllFunc();
    }
  }, []);
  // endregion
  if (api.service && api.service.data) {
    return (
      <Grid className={`service ${locale}`}>
        <Typography variant="h5">
          <FormattedMessage {...messages.header} />
        </Typography>
        <Grid container spacing={2}>
          {api.service.data.map((item, index) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={4}
              key={index.valueOf()}
              className="service-box"
            >
              <MyLink href={`/service/${item.title}`}>
                <BoxList>
                  <Box className="box-list-date">
                    {dateLocale(item.updated_at)}
                  </Box>
                  <Box className="box-list-title">{item.title}</Box>
                  <Box className="box-list-review">{item.review}</Box>
                  <Box className="box-list-Continue">
                    <FormattedMessage {...messages.Continue} />
                  </Box>
                  <img src={item.preview} alt="" />
                </BoxList>
              </MyLink>
            </Grid>
          ))}
        </Grid>
      </Grid>
    );
  }
  return '';
}

Service.propTypes = {
  api: PropTypes.object.isRequired,
};

export default memo(Service);
