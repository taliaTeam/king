/*
 * Service Messages
 *
 * This contains all the text for the Service component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Service';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'خدمات',
  },
  Continue: {
    id: `${scope}.Continue`,
    defaultMessage: 'ادامه مطلب',
  },
});
