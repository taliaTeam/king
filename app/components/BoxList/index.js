/**
 *
 * BoxList
 *
 */

import React, { memo } from 'react';
import { Box } from '@material-ui/core';
import PropTypes from 'prop-types';
import './style.scss';
function BoxList({ children }) {
  return <Box className="box-list">{children}</Box>;
}

BoxList.propTypes = {
  children: PropTypes.node.isRequired,
};

export default memo(BoxList);
