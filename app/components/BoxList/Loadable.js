/**
 *
 * Asynchronously loads the component for BoxList
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
