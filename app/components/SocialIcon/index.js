/**
 *
 * SocialIcon
 *
 */

import React, { memo } from 'react';
import IconButton from '@material-ui/core/IconButton';
import TelegramIcon from '@material-ui/icons/Telegram';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';

function SocialIcon({ justify, option }) {
  return (
    <Grid container justify={justify} className="drawer-social-icon">
      {option.telegram.value && (
        <a target="_blank" href={option.telegram.value}>
          <IconButton>
            <TelegramIcon />
          </IconButton>
        </a>
      )}
      {option.facebook.value && (
        <a target="_blank" href={option.facebook.value}>
          <IconButton>
            <FacebookIcon />
          </IconButton>
        </a>
      )}
      {option.instagram.value && (
        <a target="_blank" href={option.instagram.value}>
          <IconButton>
            <InstagramIcon />
          </IconButton>
        </a>
      )}
      {option.whatsapp.value && (
        <a target="_blank" href={option.whatsapp.value}>
          <IconButton>
            <WhatsAppIcon />
          </IconButton>
        </a>
      )}
    </Grid>
  );
}

SocialIcon.propTypes = {
  justify: PropTypes.string.isRequired,
  option: PropTypes.object.isRequired,
};

export default memo(SocialIcon);
