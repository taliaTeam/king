/**
 *
 * MyBox
 *
 */

import React, { memo } from 'react';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import './style.scss';
function MyBox({ children, className }) {
  return (
    <Paper variant="outlined" className={`my-box elem-rtl ${className}`}>
      {children}
    </Paper>
  );
}

MyBox.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

export default memo(MyBox);
