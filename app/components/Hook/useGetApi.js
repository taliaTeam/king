/**
 *
 * useGetApi
 *
 */

import { useCallback, useEffect } from 'react';
import useAxios from 'axios-hooks';

function useGetApi(dispatch, query) {
  const [responseApi, Api] = useAxios({}, { manual: true });
  useEffect(() => {
    if (responseApi.data) {
      dispatch(responseApi.data);
    }
    if (responseApi.loading) {
      dispatch(['loading']);
    }
  }, [responseApi]);
  return useCallback((currentQuery = query) => Api(currentQuery), []);
}

useGetApi.propTypes = {};

export default useGetApi;
