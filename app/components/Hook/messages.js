/*
 * Hook Messages
 *
 * This contains all the text for the Hook component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Hook';

export default defineMessages({});
