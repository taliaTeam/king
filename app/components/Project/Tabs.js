/**
 *
 * TAbs
 *
 */

import React, { memo, useContext } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import './style.scss';
import Grid from '@material-ui/core/Grid';
import { FormattedMessage } from 'react-intl';
import MyLink from '../MyLink';
import BoxList from '../BoxList';
import messages from './messages';
import ReactSlickResponsive from '../ReactSlickResponsive';
import { LangContext } from '../Context/Context';

function TabPanel({ value, index, filter, api }) {
  const { locale, dateLocale } = useContext(LangContext);
  return (
    value === index && (
      <>
        <ReactSlickResponsive>
          {[...api.project.data]
            .reverse()
            .filter(item => (filter ? item.type_project === filter : true))
            .map((item, indexMap) => (
              <Grid
                item
                key={indexMap.valueOf()}
                xs={12}
                sm={11}
                className={`project-Box ${locale}`}
              >
                <MyLink href={`/project/${item.title}`}>
                  <BoxList>
                    <Box className="box-list-date">
                      {dateLocale(item.updated_at)}
                    </Box>
                    <Box className="box-list-title">{item.title}</Box>
                    <Box className="box-list-review">
                      <FormattedMessage {...messages.code} /> {item.code}
                    </Box>
                    <Box className="box-list-Continue">
                      <FormattedMessage {...messages.Continue} />
                    </Box>
                    <img src={item.preview} alt="" />
                  </BoxList>
                </MyLink>
              </Grid>
            ))}
        </ReactSlickResponsive>
      </>
    )
  );
}

TabPanel.propTypes = {
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
  filter: PropTypes.any.isRequired,
  api: PropTypes.object.isRequired,
};

function TabsComponent({ api, value }) {
  return (
    <>
      <TabPanel value={value} index={0} api={api} filter={false} />
      <TabPanel value={value} index={1} api={api} filter="apartment" />
      <TabPanel value={value} index={2} api={api} filter="villa" />
    </>
  );
}

TabsComponent.propTypes = {
  api: PropTypes.object.isRequired,
  value: PropTypes.any.isRequired,
};

export default memo(TabsComponent);
