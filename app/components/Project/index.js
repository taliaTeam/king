/**
 *
 * Project
 *
 */

import React, { memo, useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import ApartmentIcon from '@material-ui/icons/Apartment';
import HomeIcon from '@material-ui/icons/Home';
import DialpadIcon from '@material-ui/icons/Dialpad';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TabsComponent from './Tabs';

import { ApiContext, LangContext } from '../Context/Context';
import messages from './messages';
import './style.scss';

function Project({ api }) {
  const { ProjectShowAllFunc } = useContext(ApiContext);
  const [value, setValue] = useState(0);
  const { locale } = useContext(LangContext);
  // region function
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  useEffect(() => {
    if (!api.project) {
      ProjectShowAllFunc();
    }
  }, []);
  // endregion
  if (api.project && api.project.data) {
    return (
      <Grid className={`project ${locale}`}>
        <Typography variant="h6">
          <FormattedMessage {...messages.header} />
          <span className="project-name-site">
            {api.option.data && api.option.data.name_site.value}
          </span>
        </Typography>
        <Grid>
          <AppBar position="static" color="inherit" className="app-bar-tab">
            <Tabs
              value={value}
              onChange={handleChange}
              variant="scrollable"
              scrollButtons="on"
              indicatorColor="primary"
              textColor="primary"
              aria-label="scrollable force tabs example"
            >
              <Tab
                label={<FormattedMessage {...messages.all} />}
                icon={<DialpadIcon />}
                className="project-tabs"
              />
              <Tab
                label={<FormattedMessage {...messages.apartment} />}
                icon={<ApartmentIcon />}
                className="project-tabs"
              />
              <Tab
                label={<FormattedMessage {...messages.villa} />}
                icon={<HomeIcon />}
                className="project-tabs"
              />
            </Tabs>
          </AppBar>
          <TabsComponent value={value} api={api} />
        </Grid>
      </Grid>
    );
  }
  return '';
}

Project.propTypes = {
  api: PropTypes.object.isRequired,
};

export default memo(Project);
