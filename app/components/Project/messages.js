/*
 * Project Messages
 *
 * This contains all the text for the Project component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Project';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'پروژه های بزرگ',
  },
  Continue: {
    id: `${scope}.Continue`,
    defaultMessage: 'ادامه مطلب',
  },
  code: {
    id: `${scope}.code`,
    defaultMessage: 'کذ',
  },
  all: {
    id: `${scope}.all`,
    defaultMessage: 'همه',
  },
  apartment: {
    id: `${scope}.apartment`,
    defaultMessage: 'آپارتمانی',
  },
  villa: {
    id: `${scope}.villa`,
    defaultMessage: 'ویلایی',
  },
});
