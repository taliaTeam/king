/**
 *
 * ReactSlickResponsive
 *
 */

import React, { memo } from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import './style.scss';

function ReactSlickResponsive({ children }) {
  const settings = {
    dots: true,
    infinite: false,
    className: 'react-slick-responsive',
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 960,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  return <Slider {...settings}>{children}</Slider>;
}

ReactSlickResponsive.propTypes = {
  children: PropTypes.node.isRequired,
};

export default memo(ReactSlickResponsive);
