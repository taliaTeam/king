/**
 *
 * Asynchronously loads the component for ReactSlickResponsive
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
