/**
 *
 * RouterComponent
 *
 */

import React, { memo } from 'react';
import { Switch, Route } from 'react-router-dom';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import ContentUs from 'containers/ContentUs/Loadable';
import PageServiceArticle from '../../containers/PageServiceArticle/Loadable';
import PageProject from '../../containers/PageProject/Loadable';
import Help from '../../containers/Help/Loadable';
import ArticlePage from '../../containers/ArticlePage/Loadable';
import About from '../../containers/About/Loadable';

function RouterComponent() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/service/:title" component={PageServiceArticle} />
      <Route exact path="/article" component={ArticlePage} />
      <Route path="/article/:title" component={PageServiceArticle} />
      <Route path="/project/:title" component={PageProject} />
      <Route exact path="/content-us" component={ContentUs} />
      <Route exact path="/free-help" component={Help} />
      <Route exact path="/about" component={About} />
      <Route component={NotFoundPage} />
    </Switch>
  );
}

RouterComponent.propTypes = {};

export default memo(RouterComponent);
