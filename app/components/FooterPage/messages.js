/*
 * FooterPage Messages
 *
 * This contains all the text for the FooterPage component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.FooterPage';

export default defineMessages({});
