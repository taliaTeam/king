/**
 *
 * FooterPage
 *
 */

import React, { memo, useContext } from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import './style.scss';
import { LangContext } from '../Context/Context';

function FooterPage({ api }) {
  const { locale } = useContext(LangContext);
  return (
    <Typography
      className={`footer_page ${locale}`}
      dangerouslySetInnerHTML={{
        __html:
          api.option && api.option.data && api.option.data.page_footer.value,
      }}
    />
  );
}

FooterPage.propTypes = {
  api: PropTypes.object.isRequired,
};

export default memo(FooterPage);
