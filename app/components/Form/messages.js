/*
 * Form Messages
 *
 * This contains all the text for the Form component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Form';

export default defineMessages({
  name: {
    id: `${scope}.name`,
    defaultMessage: 'نام',
  },
  phone: {
    id: `${scope}.phone`,
    defaultMessage: 'شماره تلفن',
  },
  email: {
    id: `${scope}.email`,
    defaultMessage: 'آدرس ایمیل',
  },
  message: {
    id: `${scope}.message`,
    defaultMessage: 'لطفاً پیام خود را به طور کامل وارد کنید',
  },
  subject: {
    id: `${scope}.subject`,
    defaultMessage: 'موضوع',
  },
  send: {
    id: `${scope}.send`,
    defaultMessage: 'ارسال',
  },
});
