/**
 *
 * Form
 *
 */

import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Fab from '@material-ui/core/Fab';
import SendIcon from '@material-ui/icons/Send';
import messages from './messages';
import './style.scss';

function Form({ value, setValue, submit }) {
  return (
    <form onSubmit={submit}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6} lg={4}>
          <TextField
            label={<FormattedMessage {...messages.name} />}
            variant="filled"
            required
            id="name"
            onChange={e => setValue({ ...value, name: e.currentTarget.value })}
            className="input-style"
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={4}>
          <TextField
            label={<FormattedMessage {...messages.phone} />}
            variant="filled"
            required
            id="phone"
            onChange={e => setValue({ ...value, phone: e.currentTarget.value })}
            className="input-style"
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={4}>
          <TextField
            label={<FormattedMessage {...messages.email} />}
            variant="filled"
            required
            id="email"
            onChange={e => setValue({ ...value, email: e.currentTarget.value })}
            className="input-style"
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={4}>
          <TextField
            label={<FormattedMessage {...messages.subject} />}
            variant="filled"
            required
            id="subject"
            onChange={e =>
              setValue({ ...value, subject: e.currentTarget.value })
            }
            className="input-style"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            label={<FormattedMessage {...messages.message} />}
            variant="filled"
            required
            id="content"
            rows="6"
            onChange={e =>
              setValue({ ...value, content: e.currentTarget.value })
            }
            multiline
            className="input-style"
          />
        </Grid>
        <Grid item xs={12}>
          <Fab type="submit" variant="extended" className="page-form-send">
            <SendIcon /> <FormattedMessage {...messages.send} />
          </Fab>
        </Grid>
      </Grid>
    </form>
  );
}

Form.propTypes = {
  value: PropTypes.object.isRequired,
  setValue: PropTypes.func.isRequired,
  submit: PropTypes.func.isRequired,
};

export default memo(Form);
