/**
 *
 * Slider
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import MyBox from 'components/MyBox';
import { FormattedMessage } from 'react-intl';
import Slider from 'react-slick';
import { Card } from '@material-ui/core';
import messages from './messages';
import './style.scss';
import MyLink from '../MyLink';

function SliderComponent({ api }) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    className: 'slider',
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplaySpeed: 500000,
  };
  if (api.service && api.service.data) {
    return (
      <Grid container className="slider-provider">
        <Slider {...settings} key={api.service}>
          {api.service.data.map((item, index) => (
            <Card key={index.valueOf()} className="slider-body">
              <img src={item.preview} alt="" />
              <MyBox className="box">
                <p className="box-p">{item.title}</p>
                <p className="box-p">{item.review}</p>
                <MyLink href={`/service/${item.title}`}>
                  <small>
                    <FormattedMessage {...messages.more} />
                  </small>
                </MyLink>
              </MyBox>
            </Card>
          ))}
        </Slider>
      </Grid>
    );
  }
  return '';
}

SliderComponent.propTypes = {
  api: PropTypes.object.isRequired,
};

export default SliderComponent;
