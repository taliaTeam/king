/*
 * Slider Messages
 *
 * This contains all the text for the Slider component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Slider';

export default defineMessages({
  more: {
    id: `${scope}.more`,
    defaultMessage: 'مشاهده بیشتر',
  },
});
