/*
 * MyLink Messages
 *
 * This contains all the text for the MyLink component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.MyLink';

export default defineMessages({});
