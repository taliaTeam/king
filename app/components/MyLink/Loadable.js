/**
 *
 * Asynchronously loads the component for MyLink
 *
 */

import loadable from 'utils/loadable';

export default loadable(() => import('./index'));
