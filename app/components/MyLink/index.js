/**
 *
 * MyLink
 *
 */

import React, { memo } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import './style.scss';

function MyLink({ href, children, history, className }) {
  const forward = e => {
    e.preventDefault();
    document.documentElement.scrollTop = 0;
    const hrefSplit = href.replace(/^[a-z]+:\/\/[a-z0-9:.]+/i, '');
    setTimeout(() => {
      history.push(hrefSplit);
    }, 0);
  };
  return (
    <a onClick={forward} className={`${className} my-link`} href={href}>
      {children}
    </a>
  );
}

MyLink.propTypes = {
  href: PropTypes.string.isRequired,
  history: PropTypes.object.isRequired,
  children: PropTypes.node,
  className: PropTypes.any,
};

export default withRouter(memo(MyLink));
