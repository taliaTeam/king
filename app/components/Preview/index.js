/**
 *
 * Preview
 *
 */

import React, { memo } from 'react';
// import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
// import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import image from 'images/icon-512x512.png';
import makeSelectPreview from './selectors';
import './style.scss';

function Preview() {
  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center"
      className="preview"
    >
      <img src={image} alt="" className="preview-icon ld ld-bounce" />
    </Grid>
  );
}

Preview.propTypes = {
  // api: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectPreview(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(Preview);
