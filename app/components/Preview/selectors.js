import { createSelector } from 'reselect';
/**
 * Direct selector to the apiProvider state domain
 */

const selectApiPreview = state => state.apiProvider || {};

/**
 * Other specific selectors
 */

/**
 * Default selector used by ApiProvider
 */

const makeSelectPreview = () =>
  createSelector(
    selectApiPreview,
    substate => substate,
  );

export default makeSelectPreview;
export { selectApiPreview };
