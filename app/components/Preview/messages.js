/*
 * Preview Messages
 *
 * This contains all the text for the Preview component.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.components.Preview';

export default defineMessages({});
