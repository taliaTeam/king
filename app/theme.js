import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'unset',
  },
  overrides: {},
});

export default theme;
