/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import ApiProvider from 'containers/ApiProvider';
import RouterComponent from 'components/RouterComponent';
import 'global-style.scss';

export default function App() {
  return (
    <>
      <ApiProvider>
        <RouterComponent />
      </ApiProvider>
    </>
  );
}
