/*
 * About
 *
 *
 */

import React, { memo, useContext } from 'react';
import { compose } from 'redux';
import './style.scss';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import MyBox from 'components/MyBox';
import CircularProgress from '@material-ui/core/CircularProgress';
import messages from './messages';
import MyLink from '../../components/MyLink';
import { LangContext } from '../../components/Context/Context';

function About({ api }) {
  const { locale } = useContext(LangContext);
  if (api && api.data) {
    return (
      <Grid container spacing={2} className="about">
        <Grid item xs={12} sm={5} className="preview-about">
          <img src={api.data.preview.value} alt="" />
        </Grid>
        <Grid item xs={12} sm={7} className={locale}>
          <Typography className={`about-me-title ${locale}`}>
            <FormattedMessage {...messages.about} />
          </Typography>
          <Typography
            className="text-limit-class"
            dangerouslySetInnerHTML={{
              __html: api.data.about.value,
            }}
          />
          <MyLink href="/about">
            <Typography className="more-about">
              <FormattedMessage {...messages.more} /> ...
            </Typography>
          </MyLink>
          <MyBox className="experience">
            {api.data.experience.value}
            <Typography className="experience-about">
              <FormattedMessage {...messages.experience} />
            </Typography>
          </MyBox>
        </Grid>
      </Grid>
    );
  }
  return <CircularProgress />;
}
About.propTypes = {
  api: PropTypes.any,
};
export default compose(memo)(About);
