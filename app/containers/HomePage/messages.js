/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.HomePage';

export default defineMessages({
  about: {
    id: `${scope}.about`,
    defaultMessage: 'درباره ما',
  },
  more: {
    id: `${scope}.more`,
    defaultMessage: 'بیشتر',
  },
  experience: {
    id: `${scope}.experience`,
    defaultMessage: 'سال تجربه کاری',
  },
});
