import { createSelector } from 'reselect';
import { initialState } from 'containers/ApiProvider/reducer';

/**
 * Direct selector to the HomePage state domain
 */

const selectApiDomain = state => state.apiProvider || initialState;
/**
 * Other specific selectors
 */

/**
 * Default selector used by Api
 */

const makeSelectHomePage = () =>
  createSelector(
    selectApiDomain,
    substate => substate,
  );
export default makeSelectHomePage;
