/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 */

import React, { memo } from 'react';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import './style.scss';
import PropTypes from 'prop-types';
import Navbar from 'containers/Navbar';
import SliderComponent from 'components/Slider';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Article from 'components/Article';
import { Helmet } from 'react-helmet';
import makeSelectHomePage from './selectors';
import About from './about';
import Project from '../../components/Project';
import Video from '../../components/Video';
import Footer from '../Footer';

function HomePage({ api }) {
  return (
    <div>
      {api.option && api.option.data && (
        <Helmet>
          <title>{`${api.option.data.name_site.value}`}</title>
          <meta name="description" content={api.option.data.review.value} />
        </Helmet>
      )}
      <Navbar />
      <Container maxWidth="lg">
        <SliderComponent api={api} />
        <Grid>
          {api.option && (
            <Typography className="site-details">
              {api.option.name_site && api.option.name_site.value}
            </Typography>
          )}
          {api.option && (
            <Typography className="site-details-review elem-rtl">
              {api.option && api.option.review && api.option.review.value}
            </Typography>
          )}
        </Grid>
        <About api={api.option} />
        <Project api={api} />
        <Video api={api} />
        <Article api={api} />
      </Container>
      <Footer />
    </div>
  );
}
HomePage.propTypes = {
  api: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectHomePage(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(HomePage);
