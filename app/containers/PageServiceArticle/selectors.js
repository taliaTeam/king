import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the pageServiceArticle state domain
 */

const selectPageServiceArticleDomain = state =>
  state.apiProvider || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by PageServiceArticle
 */

const makeSelectPageServiceArticle = () =>
  createSelector(
    selectPageServiceArticleDomain,
    substate => substate,
  );

export default makeSelectPageServiceArticle;
export { selectPageServiceArticleDomain };
