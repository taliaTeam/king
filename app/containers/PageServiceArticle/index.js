/**
 *
 * PageServiceArticle
 *
 */

import React, { memo, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Container } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';

import { useInjectReducer } from 'utils/injectReducer';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import makeSelectPageServiceArticle from './selectors';

import reducer from './reducer';
import { ApiContext, LangContext } from '../../components/Context/Context';
import Navbar from '../Navbar';
import Footer from '../Footer';
import './style.scss';
import FooterPage from '../../components/FooterPage';
import RelatedPost from '../../components/RelatedPost';

export function PageServiceArticle({ api, match }) {
  useInjectReducer({ key: 'pageServiceArticle', reducer });
  const { locale } = useContext(LangContext);
  const { PageShowFunc } = useContext(ApiContext);
  const page = match.url.split('/')[1];
  useEffect(() => {
    if (page === 'service') {
      PageShowFunc({
        method: 'get',
        url: `/service/show/${match.params.title}`,
      });
    }
    if (page === 'article') {
      PageShowFunc({
        method: 'get',
        url: `/article/show/${match.params.title}`,
      });
    }
  }, [window.location.href]);
  if (api.page && api.page.data) {
    return (
      <div>
        {api.option && api.option.data && (
          <Helmet>
            <title>{`${api.option.data.name_site.value} | ${
              api.page.data.title
            }`}</title>
            <meta name="description" content={api.option.data.review.value} />
          </Helmet>
        )}
        <Navbar />
        <Container>
          <Typography variant="h5" className="page-title">
            {api.page.data.title}
          </Typography>
          <img className="page-preview" src={api.page.data.preview} alt="" />
          <Grid container className={`${locale} page-content`}>
            <Grid item sm={12} md={4}>
              <Typography
                variant="inherit"
                className="page-title-content border-top"
              >
                {api.page.data.title}
              </Typography>
            </Grid>
            <Grid item sm={12} md={8}>
              <div
                dangerouslySetInnerHTML={{
                  __html: api.page.data.content,
                }}
              />
            </Grid>
          </Grid>
          <FooterPage api={api} />
          <RelatedPost posts={api.page} type={page} />
        </Container>
        <Footer />
      </div>
    );
  }
  return <CircularProgress />;
}

PageServiceArticle.propTypes = {
  api: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectPageServiceArticle(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(PageServiceArticle);
