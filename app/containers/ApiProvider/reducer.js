/*
 *
 * ApiProvider reducer
 *
 */
import produce from 'immer';
import {
  SET_PARAM_BY_API,
  SET_NULL_PARAM_BY_API,
  SET_NULL_API,
} from './constants';

const init = {
  option: null,
  language: null,
  service: null,
  project: null,
  video: null,
  article: null,
  page: null,
  content: null,
};
export const initialState = { ...init };

/* eslint-disable default-case, no-param-reassign */
const apiProviderReducer = (state = initialState, action) =>
  produce(state, draft => {
    switch (action.type) {
      case SET_PARAM_BY_API:
        draft[action.paramKey] = action.paramValue;
        break;
      case SET_NULL_PARAM_BY_API:
        draft[action.paramKey] = null;
        break;
      case SET_NULL_API:
        draft.option = null;
        draft.language = null;
        draft.service = null;
        draft.project = null;
        draft.video = null;
        draft.article = null;
        draft.page = null;
        break;
    }
  });

export default apiProviderReducer;
