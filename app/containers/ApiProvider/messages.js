/*
 * ApiProvider Messages
 *
 * This contains all the text for the ApiProvider container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ApiProvider';

export default defineMessages({});
