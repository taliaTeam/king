/**
 *
 * ApiProvider
 *
 */

import React, { memo, useMemo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectReducer } from 'utils/injectReducer';
import useGetApi from 'components/Hook/useGetApi';
import { ApiContext } from 'components/Context/Context';
import makeSelectApiProvider from './selectors';
import reducer from './reducer';
import { setParamByApiAction } from './actions';
import Preview from '../../components/Preview/Loadable';

export function ApiProvider({ children, setParamByApiActionFunc }) {
  useInjectReducer({ key: 'apiProvider', reducer });
  const [showPage, setShowPage] = useState(false);
  useEffect(() => {
    // eslint-disable-next-line no-unused-expressions
    !showPage &&
      setTimeout(() => {
        setShowPage(true);
      }, 3000);
  }, [showPage]);
  const dispatchApi = paramKey => paramValue =>
    setParamByApiActionFunc(paramKey, paramValue);
  const LanguageShowAllFunc = useGetApi(dispatchApi('language'), {
    method: 'get',
    url: '/language/show-all',
  });
  const OptionFunc = useGetApi(dispatchApi('option'), {
    method: 'get',
    url: '/option/show-all',
  });
  const ServiceShowAllFunc = useGetApi(dispatchApi('service'), {
    method: 'get',
    url: '/service/show-all',
  });
  const ProjectShowAllFunc = useGetApi(dispatchApi('project'), {
    method: 'get',
    url: '/project/show-all',
  });
  const VideoShowAllFunc = useGetApi(dispatchApi('video'), {
    method: 'get',
    url: '/video/show-all',
  });
  const ArticleShowAllFunc = useGetApi(dispatchApi('article'), {
    method: 'get',
    url: '/article/show-all',
  });
  const ContentFunc = useGetApi(dispatchApi('content'), {
    method: 'get',
    url: '/article/show-all',
  });
  const PageShowFunc = useGetApi(dispatchApi('page'), {});
  const render = useMemo(
    () => ({
      LanguageShowAllFunc,
      OptionFunc,
      ServiceShowAllFunc,
      ProjectShowAllFunc,
      VideoShowAllFunc,
      ArticleShowAllFunc,
      PageShowFunc,
      ContentFunc,
    }),
    [],
  );
  if (showPage) {
    return (
      <ApiContext.Provider value={{ ...render, setShowPage }}>
        {children}
      </ApiContext.Provider>
    );
  }
  return (
    <ApiContext.Provider value={render}>
      <Preview />
    </ApiContext.Provider>
  );
}

ApiProvider.propTypes = {
  children: PropTypes.object.isRequired,
  setParamByApiActionFunc: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectApiProvider(),
});

function mapDispatchToProps(dispatch) {
  return {
    setParamByApiActionFunc: (paramKey, paramValue) =>
      dispatch(setParamByApiAction(paramKey, paramValue)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(ApiProvider);
