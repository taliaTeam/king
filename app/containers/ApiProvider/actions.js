/*
 *
 * ApiProvider actions
 *
 */

import {
  SET_PARAM_BY_API,
  SET_NULL_PARAM_BY_API,
  SET_NULL_API,
} from './constants';

export function setParamByApiAction(paramKey, paramValue) {
  return {
    type: SET_PARAM_BY_API,
    paramKey,
    paramValue,
  };
}

export function setNullParamByApiAction(paramKey) {
  return {
    type: SET_NULL_PARAM_BY_API,
    paramKey,
  };
}

export function setNullApiAction() {
  return {
    type: SET_NULL_API,
  };
}
