/*
 *
 * ApiProvider constants
 *
 */

export const SET_PARAM_BY_API = 'app/Api/SET_PARAM_BY_API';
export const SET_NULL_PARAM_BY_API = 'app/Api/SET_NULL_PARAM_BY_API';
export const SET_NULL_API = 'app/Api/SET_NULL_API';
