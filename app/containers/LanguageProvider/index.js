/*
 *
 * LanguageProvider
 *
 * this component connects the redux state language locale to the
 * IntlProvider component and i18n messages (loaded from `app/translations`)
 */

import React, { useEffect, useMemo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { IntlProvider } from 'react-intl';

import JDate from 'jalali-date';
import { makeSelectLocale } from './selectors';
import { changeLocale } from './actions';
import { LangContext } from '../../components/Context/Context';

export function LanguageProvider({ changeLocalFunc, ...props }) {
  let language = localStorage.getItem('language');
  useEffect(() => {
    if (language === null) {
      localStorage.setItem('language', 'fa');
      language = 'fa';
    }
    changeLocalFunc(language);
  }, []);

  const locale = useMemo(
    () => (['fa', 'ar'].indexOf(props.locale) > -1 ? 'elem-rtl' : 'elem-ltr'),
    [props.locale],
  );
  const dateLocale = useCallback(
    date => {
      if (['fa'].indexOf(props.locale) > -1) {
        return new JDate(new Date(`${date.replace(/\s+/g, 'T')}.000Z`)).format(
          'DD MMMM YYYY',
        );
      }
      return new Date(`${date.replace(/\s+/g, 'T')}.000Z`)
        .toISOString()
        .slice(0, 10);
    },
    [props.locale],
  );
  return (
    <IntlProvider
      locale={props.locale}
      key={props.locale}
      messages={props.messages[props.locale]}
    >
      <LangContext.Provider value={{ locale, dateLocale }}>
        {React.Children.only(props.children)}
      </LangContext.Provider>
    </IntlProvider>
  );
}

LanguageProvider.propTypes = {
  locale: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
  changeLocalFunc: PropTypes.func.isRequired,
};

const mapStateToProps = createSelector(
  makeSelectLocale(),
  locale => ({
    locale,
  }),
);

function mapDispatchToProps(dispatch) {
  return {
    changeLocalFunc: local => dispatch(changeLocale(local)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LanguageProvider);
