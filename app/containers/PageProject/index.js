/**
 *
 * PageProject
 *
 */

import React, { memo, useContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Container } from '@material-ui/core';
import ZoomOutMapIcon from '@material-ui/icons/ZoomOutMap';
import Typography from '@material-ui/core/Typography';
import { FormattedMessage } from 'react-intl';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import Drawer from '@material-ui/core/Drawer';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import makeSelectPagePageProject from './selectors';
import { ApiContext, LangContext } from '../../components/Context/Context';
import Navbar from '../Navbar';
import Footer from '../Footer';
import './style.scss';
import FooterPage from '../../components/FooterPage';
import RelatedPost from '../../components/RelatedPost';
import messages from './messages';
import BoxList from '../../components/BoxList';
import SliderP from './Slider';

export function PageProject({ api, match }) {
  const { PageShowFunc } = useContext(ApiContext);
  const { locale } = useContext(LangContext);
  const [images, setImages] = useState([]);
  const [currentImage, setCurrentImage] = useState(0);
  const [state, setState] = useState({
    top: false,
  });
  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };
  const fullList = side => (
    <div
      className="image-player"
      role="presentation"
      onKeyDown={toggleDrawer(side, false)}
    >
      <Button className="close-player" onClick={toggleDrawer(side, false)}>
        <CloseIcon />
      </Button>
      <Container>
        <Grid container direction="row" justify="center" alignItems="center">
          <SliderP api={api} currentImage={currentImage} images={images} />
        </Grid>
      </Container>
    </div>
  );
  useEffect(() => {
    PageShowFunc({
      method: 'get',
      url: `/project/show/${match.params.title}`,
    });
  }, [window.location.href]);
  useEffect(() => {
    if (api.page && api.page.data && api.page.data.images) {
      const image = JSON.parse(api.page.data.images);
      if (image && image instanceof Array) {
        setImages(image);
      }
    }
  }, [api]);
  if (api.page && api.page.data) {
    return (
      <div>
        {api.option && api.option.data && (
          <Helmet>
            <title>{`${api.option.data.name_site.value} | ${
              api.page.data.title
            }`}</title>
            <meta name="description" content={api.option.data.review.value} />
          </Helmet>
        )}
        <Navbar />
        <Container>
          <Typography variant="h5" className="page-title">
            {api.page.data.title}
            <Typography variant="inherit" className="page-title-code">
              <FormattedMessage {...messages.code} /> {api.page.data.code}
            </Typography>
          </Typography>
          <Grid className={`${locale} page-review`}>
            <Typography
              variant="subtitle2"
              dangerouslySetInnerHTML={{
                __html: api.page.data.content,
              }}
            />
          </Grid>

          <Grid container spacing={2} className={`${locale} page-content`}>
            {images[0] &&
              images.map((item, index) => (
                <Grid
                  key={index.valueOf()}
                  item
                  xs={12}
                  sm={6}
                  className="page-element-image"
                >
                  <BoxList>
                    <img src={item} alt="" />
                    <Grid
                      onClick={e => {
                        setCurrentImage(index);
                        toggleDrawer('top', true)(e);
                      }}
                      container
                      direction="row"
                      justify="center"
                      alignContent="center"
                      className="page-zoom-icon"
                    >
                      <ZoomOutMapIcon />
                    </Grid>
                  </BoxList>
                </Grid>
              ))}
          </Grid>
          <FooterPage api={api} />
          <RelatedPost posts={api.page} />
        </Container>
        <Drawer
          anchor="top"
          open={state.top}
          onClose={toggleDrawer('top', false)}
        >
          {fullList('top')}
        </Drawer>
        <Footer />
      </div>
    );
  }
  return <CircularProgress />;
}

PageProject.propTypes = {
  api: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectPagePageProject(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(PageProject);
