/**
 *
 * Slider
 *
 */

import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import ImageGallery from 'react-image-gallery';

export function SliderP({ api, currentImage, images }) {
  const [image, setImage] = useState([]);
  useEffect(() => {
    setImage(
      images.map(item => ({
        original: item,
        thumbnail: item,
      })),
    );
  }, [images]);
  return (
    <>
      <ImageGallery
        items={image}
        showIndex
        startIndex={currentImage}
        onClick={e => console.log(e)}
      />
    </>
  );
}

SliderP.propTypes = {
  api: PropTypes.object.isRequired,
  currentImage: PropTypes.number.isRequired,
  images: PropTypes.array.isRequired,
};

export default memo(SliderP);
