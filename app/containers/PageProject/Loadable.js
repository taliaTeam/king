/**
 *
 * Asynchronously loads the component for PageServiceArticle
 *
 */
import React from 'react';
import loadable from 'utils/loadable';
import Preview from '../../components/Preview';

export default loadable(() => import('./index'), {
  fallback: <Preview />,
});
