/*
 * PageServiceArticle Messages
 *
 * This contains all the text for the PageServiceArticle container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.PageProject';

export default defineMessages({
  code: {
    id: `${scope}.code`,
    defaultMessage: 'کد',
  },
});
