import { createSelector } from 'reselect';

/**
 * Direct selector to the pageServiceArticle state domain
 */

const selectPageProjectDomain = state => state.apiProvider || {};

/**
 * Other specific selectors
 */

/**
 * Default selector used by PageServiceArticle
 */

const makeSelectPagePageProject = () =>
  createSelector(
    selectPageProjectDomain,
    substate => substate,
  );

export default makeSelectPagePageProject;
export { selectPageProjectDomain };
