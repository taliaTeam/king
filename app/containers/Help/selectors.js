import { createSelector } from 'reselect';

/**
 * Direct selector to the contentUs state domain
 */

const selectHelpDomain = state => state.apiProvider || {};

/**
 * Other specific selectors
 */

/**
 * Default selector used by Help
 */

const makeSelectHelp = () =>
  createSelector(
    selectHelpDomain,
    substate => substate,
  );

export default makeSelectHelp;
export { selectHelpDomain };
