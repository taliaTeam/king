/*
 * ContentUs Messages
 *
 * This contains all the text for the ContentUs container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ContentUs';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'مشاوره رایگان',
  },
  help: {
    id: `${scope}.help`,
    defaultMessage: 'درخواست مشاوره رایگان اقامت ترکیه',
  },
  success: {
    id: `${scope}.success`,
    defaultMessage: 'پیام با موفقیت ارسال شد',
  },
});
