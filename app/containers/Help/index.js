/**
 *
 * Help
 *
 */

import React, { memo, useContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import 'mapbox-gl/dist/mapbox-gl.css';
import Grid from '@material-ui/core/Grid';
import { Container } from '@material-ui/core';
import makeSelectHelp from './selectors';
import messages from './messages';
import Navbar from '../Navbar';
import Footer from '../Footer';
import './style.scss';
import { ApiContext, LangContext } from '../../components/Context/Context';
import Form from '../../components/Form';
import { setNullParamByApiAction } from '../ApiProvider/actions';

export function Help({ api, setNullResponseForm }) {
  const { ContentFunc } = useContext(ApiContext);
  const { locale } = useContext(LangContext);
  const [form, setForm] = useState({});
  const [open, setOpen] = React.useState(false);
  const [message, setMessage] = React.useState('');

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  useEffect(() => {
    if (api.content) {
      if (api.content[0] === 'loading') {
        setMessage('loading');
      } else {
        setMessage(<FormattedMessage {...messages.success} />);
        setNullResponseForm('content');
      }
      setOpen(true);
    }
  }, [api]);
  const submitForm = e => {
    e.preventDefault();
    ContentFunc({
      method: 'post',
      data: {
        name: form.name,
        phone: form.phone,
        email: form.email,
        subject: form.subject,
        content: form.content,
        type: 'help',
      },
      url: '/content/create',
    });
  };
  return (
    <div>
      {api.option && api.option.data && (
        <Helmet>
          <title>{`${api.option.data.name_site.value}`}</title>
          <meta name="description" content={api.option.data.review.value} />
        </Helmet>
      )}
      <Navbar />
      <Container>
        <Typography variant="h5" className="page-title">
          <FormattedMessage {...messages.header} />
        </Typography>
        <Grid container className={`${locale} page-content`}>
          <Grid item xs={12} md={4}>
            <Typography
              variant="inherit"
              className="page-title-for-us border-top"
            >
              <FormattedMessage {...messages.help} />
            </Typography>
          </Grid>
          <Grid item xs={12} md={8}>
            {api.option && api.option.data && (
              <Typography
                variant="subtitle2"
                dangerouslySetInnerHTML={{
                  __html: api.option.data.help.value,
                }}
              />
            )}
          </Grid>
        </Grid>
        <Grid container className={`${locale} page-content`}>
          <Grid item xs={12} md={4} />
          <Grid item xs={12} md={8} className="page-field">
            <Form value={form} setValue={setForm} submit={submitForm} />
          </Grid>
        </Grid>
        <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
          <MuiAlert
            elevation={6}
            variant="filled"
            severity="success"
            onClose={handleClose}
          >
            {message}
          </MuiAlert>
        </Snackbar>
      </Container>
      <Footer />
    </div>
  );
}

Help.propTypes = {
  api: PropTypes.object.isRequired,
  setNullResponseForm: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectHelp(),
});

function mapDispatchToProps(dispatch) {
  return {
    setNullResponseForm: paramKey =>
      dispatch(setNullParamByApiAction(paramKey)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(Help);
