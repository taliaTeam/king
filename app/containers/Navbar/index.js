/**
 *
 * Navbar
 *
 */

import React, { memo, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import TranslateTwoToneIcon from '@material-ui/icons/TranslateTwoTone';

import { useInjectReducer } from 'utils/injectReducer';
import './style.scss';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import MyLink from 'components/MyLink';
import reducer from './reducer';
import makeSelectNavbar, { makeSelectLanguage } from './selectors';
import { ApiContext } from '../../components/Context/Context';
import { DrawerCom } from './drower';
import { changeLocale } from '../LanguageProvider/actions';
import { setNullApiAction } from '../ApiProvider/actions';

export function Navbar({
  api,
  changeLocalFunc,
  language,
  setNullApi,
  history,
}) {
  useInjectReducer({ key: 'navbar', reducer });
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);
  const { LanguageShowAllFunc, OptionFunc, setShowPage } = useContext(
    ApiContext,
  );
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const classes = useStyles();

  // region function
  useEffect(() => {
    if (!api.language) {
      LanguageShowAllFunc();
    }
    if (!api.option) {
      OptionFunc();
    }
  }, []);

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const languageFunc = () =>
    api.language &&
    api.language.data &&
    api.language.data
      .filter(item => item.short !== language.locale)
      .map((item, index) => (
        <IconButton
          onClick={() => {
            setShowPage(false);
            history.push('/');
            changeLocalFunc(item.short);
            setNullApi();
          }}
          color="inherit"
          key={index.valueOf()}
          className="navbar-flag"
        >
          <img src={item.flag} alt="" className="navbar-flag-img" />
        </IconButton>
      ));

  // endregion

  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>{languageFunc()}</MenuItem>
    </Menu>
  );

  return (
    <div className={`${classes.root} navbar-static`}>
      <AppBar position="static" className="nav-color navbar elem-rtl">
        <Toolbar>
          <DrawerCom api={api} />
          <div className={classes.sectionDesktop}>{languageFunc()}</div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <TranslateTwoToneIcon />
            </IconButton>
          </div>
          {api.option && api.option.data && api.option.data.phone.value && (
            <a
              className="navbar-phone"
              target="_blank"
              href={`tel:+${api.option.data.phone.value}`}
            >
              {api.option.data.phone.value}
            </a>
          )}
          <IconButton color="inherit" className="navbar-flag-logo-link">
            {api.option && api.option.data && (
              <MyLink href="/">
                <img
                  key={api.option}
                  src={api.option.data.logo.value}
                  alt=""
                  className="navbar-flag-logo ld ld-flip-h duration-3"
                />
              </MyLink>
            )}
          </IconButton>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
    </div>
  );
}

Navbar.propTypes = {
  api: PropTypes.object.isRequired,
  changeLocalFunc: PropTypes.func.isRequired,
  language: PropTypes.object.isRequired,
  setNullApi: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectNavbar(),
  language: makeSelectLanguage(),
});

function mapDispatchToProps(dispatch) {
  return {
    changeLocalFunc: local => dispatch(changeLocale(local)),
    setNullApi: () => dispatch(setNullApiAction()),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
  withRouter,
)(Navbar);

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
}));
