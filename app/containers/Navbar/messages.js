/*
 * Navbar Messages
 *
 * This contains all the text for the Navbar container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Navbar';

export default defineMessages({
  copyright: {
    id: `${scope}.copyright`,
    defaultMessage: 'تمامی حقوق این سایت محفوظ است',
  },
  contentUs: {
    id: `${scope}.contentUs`,
    defaultMessage: 'تماس با ما',
  },
  help: {
    id: `${scope}.help`,
    defaultMessage: 'مشاوره رایگان',
  },
  about: {
    id: `${scope}.about`,
    defaultMessage: 'درباره ما',
  },
});
