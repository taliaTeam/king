import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the navbar state domain
 */

const selectNavbarDomain = state => state.apiProvider || initialState;
const selectNavbarLanguage = state => state.language || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by Navbar
 */

const makeSelectNavbar = () =>
  createSelector(
    selectNavbarDomain,
    substate => substate,
  );
const makeSelectLanguage = () =>
  createSelector(
    selectNavbarLanguage,
    substate => substate,
  );

export default makeSelectNavbar;
export { selectNavbarDomain, makeSelectLanguage };
