/**
 *
 * DrawerCom
 *
 */

import React, { memo, useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';

import { useInjectReducer } from 'utils/injectReducer';
import './style.scss';
import Drawer from '@material-ui/core/Drawer';
import makeStyles from '@material-ui/core/styles/makeStyles';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import List from '@material-ui/core/List';
import MyLink from 'components/MyLink';
import { FormattedMessage } from 'react-intl';
import reducer from './reducer';
import { ApiContext } from '../../components/Context/Context';
import SocialIcon from '../../components/SocialIcon';
import messages from './messages';

export function DrawerCom({ api }) {
  useInjectReducer({ key: 'navbar', reducer });
  const { ServiceShowAllFunc } = useContext(ApiContext);
  const classes = useStyles();
  const [state, setState] = useState({ right: false });

  // region function
  useEffect(() => {
    if (!api.service) {
      ServiceShowAllFunc();
    }
  }, []);
  const sideList = side => (
    <div
      className={`${classes.list} drawer`}
      role="presentation"
      onKeyDown={toggleDrawer(side, false)}
    >
      <div className=" ld ld-slide-btt pause-1 duration-2">
        <div className="drawer-close">
          <IconButton
            className="drawer-close-icon"
            onClick={toggleDrawer(side, false)}
          >
            <CloseIcon />
          </IconButton>
        </div>
        <List className="drawer-list elem-rtl">
          {api.service &&
            api.service.data &&
            api.service.data.map((item, index) => (
              <li key={index.valueOf()} className="drawer-list-li">
                <MyLink href={`/service/${item.title}`}>{item.title}</MyLink>
              </li>
            ))}
          <li className="drawer-list-li">
            <MyLink href="/content-us">
              <FormattedMessage {...messages.contentUs} />
            </MyLink>
          </li>
          <li className="drawer-list-li">
            <MyLink href="/free-help">
              <FormattedMessage {...messages.help} />
            </MyLink>
          </li>
          <li className="drawer-list-li">
            <MyLink href="/about">
              <FormattedMessage {...messages.about} />
            </MyLink>
          </li>
        </List>
        {api.option && api.option.data && (
          <SocialIcon justify="flex-end" option={api.option.data} />
        )}
        <div className="drawer-copyright">
          © <FormattedMessage {...messages.copyright} />
        </div>
      </div>
    </div>
  );
  const toggleDrawer = (side, open) => event => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }

    setState({ ...state, [side]: open });
  };
  // endregion
  return (
    <div>
      <IconButton
        edge="start"
        className="navbar-menu-icon"
        color="inherit"
        aria-label="menu"
        onClick={toggleDrawer('right', true)}
      >
        <MenuIcon />
      </IconButton>
      <Drawer
        anchor="right"
        open={state.right}
        onClose={toggleDrawer('right', false)}
      >
        {sideList('right')}
      </Drawer>
    </div>
  );
}

DrawerCom.propTypes = {
  api: PropTypes.object.isRequired,
};

export default compose(memo)(DrawerCom);

const useStyles = makeStyles(() => ({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
}));
