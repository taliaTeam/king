/**
 *
 * About
 *
 */

import React, { memo, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import 'mapbox-gl/dist/mapbox-gl.css';
import Grid from '@material-ui/core/Grid';
import { Container } from '@material-ui/core';
import makeSelectAbout from './selectors';
import messages from './messages';
import Navbar from '../Navbar';
import Footer from '../Footer';
import './style.scss';
import { ApiContext, LangContext } from '../../components/Context/Context';

export function About({ api }) {
  const { OptionFunc } = useContext(ApiContext);
  const { locale } = useContext(LangContext);
  // region function
  useEffect(() => {
    if (!api.option) {
      OptionFunc();
    }
  }, []);
  // endregion

  if (api.option && api.option.data) {
    return (
      <div>
        {api.option && api.option.data && (
          <Helmet>
            <title>{`${api.option.data.name_site.value} | about`}</title>
            <meta name="description" content={api.option.data.review.value} />
          </Helmet>
        )}
        <Navbar />
        <Container>
          <Typography variant="h5" className="page-title">
            <FormattedMessage {...messages.header} />
          </Typography>
          <Grid className="page-preview">
            <img src={api.option.data.preview.value} alt="" />
          </Grid>
          <Grid container className={`${locale} page-content`}>
            <Grid item xs={12} md={4}>
              <Typography
                variant="inherit"
                className="page-title-for-us border-top"
              >
                <FormattedMessage {...messages.header} />
              </Typography>
            </Grid>
            <Grid item xs={12} md={8}>
              {api.option && api.option.data && (
                <Typography
                  variant="subtitle2"
                  dangerouslySetInnerHTML={{
                    __html: api.option.data.about.value,
                  }}
                />
              )}
            </Grid>
          </Grid>
        </Container>
        <Footer />
      </div>
    );
  }
  return <CircularProgress />;
}

About.propTypes = {
  api: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectAbout(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(About);
