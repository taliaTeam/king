/*
 * NotFoundPage Messages
 *
 * This contains all the text for the NotFoundPage container.
 */
import { defineMessages } from 'react-intl';

export const scope = 'app.containers.NotFoundPage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'صفحه مورد نظر یافت نشد',
  },
  message: {
    id: `${scope}.message`,
    defaultMessage: 'متاسفانه صفحه مورد نظر یافت نشد',
  },
});
