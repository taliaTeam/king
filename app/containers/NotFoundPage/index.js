/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React, { memo } from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Typography from '@material-ui/core/Typography';
import { Helmet } from 'react-helmet';
import Grid from '@material-ui/core/Grid';
import { Container } from '@material-ui/core';
import makeSelectNotFound from './selectors';
import messages from './messages';
import Footer from '../Footer';
import Navbar from '../Navbar';
import './style.scss';

function NotFound({ api, intl }) {
  return (
    <Container>
      <Helmet>
        <title>{intl.formatMessage(messages.header)}</title>
      </Helmet>
      <Grid container justify="flex-end" direction="row">
        <Navbar />
        <Grid item xs={12}>
          {api.option && api.option.data && (
            <img src={api.option.data.logo.value} alt="" />
          )}
        </Grid>
        <Typography className="notfound-title">
          <FormattedMessage {...messages.message} />
        </Typography>
        <Footer />
      </Grid>
    </Container>
  );
}
NotFound.propTypes = {
  api: PropTypes.object.isRequired,
  intl: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectNotFound(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
  injectIntl,
)(NotFound);
