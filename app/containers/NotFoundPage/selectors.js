import { createSelector } from 'reselect';

/**
 * Direct selector to the HomePage state domain
 */

const selectApiDomain = state => state.apiProvider || {};
/**
 * Other specific selectors
 */

/**
 * Default selector used by Api
 */

const makeSelectNotFound = () =>
  createSelector(
    selectApiDomain,
    substate => substate,
  );
export default makeSelectNotFound;
