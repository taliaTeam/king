/**
 *
 * ArticlePage
 *
 */

import React, { memo, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectReducer } from 'utils/injectReducer';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Box } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import JDate from 'jalali-date';
import makeSelectArticlePage from './selectors';
import reducer from './reducer';
import messages from './messages';
import Navbar from '../Navbar';
import Footer from '../Footer';
import MyLink from '../../components/MyLink';
import { ApiContext } from '../../components/Context/Context';
import BoxList from '../../components/BoxList';
import './style.scss';

export function ArticlePage({ api }) {
  useInjectReducer({ key: 'articlePage', reducer });
  const { ArticleShowAllFunc } = useContext(ApiContext);

  // region function
  useEffect(() => {
    if (!api.article) {
      ArticleShowAllFunc();
    }
  }, []);
  // endregion
  if (api.article && api.article.data) {
    return (
      <div>
        <Helmet>
          <title>ArticlePage</title>
          <meta name="description" content="Description of ArticlePage" />
        </Helmet>
        <Navbar />
        <Grid className="article-page elem-rtl">
          <Container>
            <Typography variant="h5">
              <FormattedMessage {...messages.header} />
            </Typography>
            <Grid container spacing={2}>
              {[...api.article.data].reverse().map((item, index) => (
                <Grid
                  item
                  xs={12}
                  sm={6}
                  md={4}
                  key={index.valueOf()}
                  className="article-page-box"
                >
                  <MyLink href={`/article/${item.title}`}>
                    <BoxList>
                      <Box className="box-list-date">
                        {new JDate(new Date(item.updated_at)).format(
                          'DD MMMM YYYY',
                        )}
                      </Box>
                      <Box className="box-list-title">{item.title}</Box>
                      <Box className="box-list-review">{item.review}</Box>
                      <Box className="box-list-Continue">
                        <FormattedMessage {...messages.Continue} />
                      </Box>
                      <img src={item.preview} alt="" />
                    </BoxList>
                  </MyLink>
                </Grid>
              ))}
            </Grid>
          </Container>
        </Grid>
        <Footer />
      </div>
    );
  }
}

ArticlePage.propTypes = {
  api: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectArticlePage(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(ArticlePage);
