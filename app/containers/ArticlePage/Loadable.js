/**
 *
 * Asynchronously loads the component for ArticlePage
 *
 */

import loadable from 'utils/loadable';
import React from 'react';
import Preview from '../../components/Preview';

export default loadable(() => import('./index'), {
  fallback: <Preview />,
});
