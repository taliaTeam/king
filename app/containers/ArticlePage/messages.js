/*
 * ArticlePage Messages
 *
 * This contains all the text for the ArticlePage container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ArticlePage';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'مقالات',
  },
  Continue: {
    id: `${scope}.Continue`,
    defaultMessage: 'ادامه مطلب',
  },
});
