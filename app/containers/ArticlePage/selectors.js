import { createSelector } from 'reselect';

/**
 * Direct selector to the articlePage state domain
 */

const selectArticlePageDomain = state => state.apiProvider || {};

/**
 * Other specific selectors
 */

/**
 * Default selector used by ArticlePage
 */

const makeSelectArticlePage = () =>
  createSelector(
    selectArticlePageDomain,
    substate => substate,
  );

export default makeSelectArticlePage;
export { selectArticlePageDomain };
