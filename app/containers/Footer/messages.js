/*
 * Footer Messages
 *
 * This contains all the text for the Footer container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Footer';

export default defineMessages({
  about: {
    id: `${scope}.about`,
    defaultMessage: 'درباره ما',
  },
  article: {
    id: `${scope}.article`,
    defaultMessage: 'مقالات',
  },
  service: {
    id: `${scope}.service`,
    defaultMessage: 'خدمات',
  },
  continue: {
    id: `${scope}.continue`,
    defaultMessage: 'ادامه',
  },
  copyright: {
    id: `${scope}.copyright`,
    defaultMessage: 'تمامی حقوق محفوظ است',
  },
});
