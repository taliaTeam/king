import { createSelector } from 'reselect';

/**
 * Direct selector to the footer state domain
 */

const selectFooterDomain = state => state.apiProvider || {};

/**
 * Other specific selectors
 */

/**
 * Default selector used by Footer
 */

const makeSelectFooter = () =>
  createSelector(
    selectFooterDomain,
    substate => substate,
  );

export default makeSelectFooter;
export { selectFooterDomain };
