/**
 *
 * Footer
 *
 */

import React, { memo, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { compose } from 'redux';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { createStructuredSelector } from 'reselect';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Container } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import SocialIcon from 'components/SocialIcon';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';

import messages from './messages';
import './style.scss';
import makeSelectFooter from './selectors';
import MyLink from '../../components/MyLink';
import { ApiContext, LangContext } from '../../components/Context/Context';

export function Footer({ api }) {
  const { ArticleShowAllFunc, ServiceShowAllFunc } = useContext(ApiContext);
  const { locale } = useContext(LangContext);
  // region function
  useEffect(() => {
    if (!api.article) {
      ArticleShowAllFunc();
    }
    if (!api.option) {
      ServiceShowAllFunc();
    }
  }, []);
  // endregion

  return (
    <Container>
      <Grid container direction="row" spacing={2} className="footer">
        <Grid xs={12} sm={6} md={3} item className="footer-details">
          {api.option && api.option.data && (
            <img
              src={api.option.data.logo.value}
              alt=""
              className="footer_logo"
            />
          )}
          {api.option && api.option.data && (
            <SocialIcon justify="center" option={api.option.data} />
          )}
          <Typography variant="subtitle2" align="center">
            © <FormattedMessage {...messages.copyright} />
          </Typography>
        </Grid>
        <Grid xs={12} sm={6} md={3} item className={locale}>
          <Typography variant="inherit" className="border-bottom footer-title">
            <FormattedMessage {...messages.service} />
          </Typography>
          {api.service &&
            api.service.data &&
            [...api.service.data]
              .reverse()
              .splice(0, 4)
              .map((item, index) => (
                <MyLink href={`/service/${item.title}`} key={index.valueOf()}>
                  <ListItem button className="footer-list">
                    <ListItemText primary={item.title} />
                  </ListItem>
                </MyLink>
              ))}
        </Grid>
        <Grid xs={12} sm={6} md={3} className={locale} item>
          <Typography variant="inherit" className="border-bottom footer-title">
            <FormattedMessage {...messages.article} />
          </Typography>
          {api.article &&
            api.article.data &&
            [...api.article.data]
              .reverse()
              .splice(0, 4)
              .map((item, index) => (
                <MyLink href={`/article/${item.title}`} key={index.valueOf()}>
                  <ListItem button className="footer-list">
                    <ListItemText primary={item.title} />
                  </ListItem>
                </MyLink>
              ))}
        </Grid>
        <Grid xs={12} sm={6} md={3} className={locale} item>
          <Typography variant="inherit" className="border-bottom footer-title">
            <FormattedMessage {...messages.about} />
          </Typography>
          <Typography variant="subtitle2" className="text-limit-class-justify">
            {api.option &&
              api.option.data &&
              api.option.data.about.value
                .split(' ')
                .splice(0, 84)
                .join(' ')}{' '}
            ...{' '}
            <MyLink href="/about">
              <FormattedMessage {...messages.continue} />
            </MyLink>
          </Typography>
        </Grid>
      </Grid>
      {api.option && api.option.data && api.option.data.whatsapp.value && (
        <a href={api.option.data.whatsapp.value} target="_blank">
          <IconButton color="primary" className="footer-icon">
            <WhatsAppIcon />
          </IconButton>
        </a>
      )}
    </Container>
  );
}

Footer.propTypes = {
  api: PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  api: makeSelectFooter(),
});

const withConnect = connect(mapStateToProps);

export default compose(
  withConnect,
  memo,
)(Footer);
