import { createSelector } from 'reselect';
import { initialState } from './reducer';

/**
 * Direct selector to the contentUs state domain
 */

const selectContentUsDomain = state => state.apiProvider || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ContentUs
 */

const makeSelectContentUs = () =>
  createSelector(
    selectContentUsDomain,
    substate => substate,
  );

export default makeSelectContentUs;
export { selectContentUsDomain };
