/*
 * ContentUs Messages
 *
 * This contains all the text for the ContentUs container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ContentUs';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'تماس با ما',
  },
  witing: {
    id: `${scope}.witing`,
    defaultMessage: 'منتظر شما هستیم',
  },
  forUs: {
    id: `${scope}.forUs`,
    defaultMessage: 'برای ما بنویسید',
  },
  success: {
    id: `${scope}.success`,
    defaultMessage: 'پیام با موفقیت ارسال شد',
  },
});
