/**
 * i18n.js
 *
 * This will setup the i18n language files and locale data for your app.
 *
 *   IMPORTANT: This file is used by the internal build
 *   script `extract-intl`, and must use CommonJS module syntax
 *   You CANNOT use import/export in this file.
 */
const addLocaleData = require('react-intl').addLocaleData; //eslint-disable-line
const enLocaleData = require('react-intl/locale-data/en');
const trLocaleData = require('react-intl/locale-data/tr');
const arLocaleData = require('react-intl/locale-data/ar');
const faLocaleData = require('react-intl/locale-data/fa');

const enTranslationMessages = require('./translations/en.json');
const trTranslationMessages = require('./translations/tr.json');
const arTranslationMessages = require('./translations/ar.json');
const faTranslationMessages = require('./translations/fa.json');

addLocaleData(enLocaleData);
addLocaleData(faLocaleData);
addLocaleData(trLocaleData);
addLocaleData(arLocaleData);

const DEFAULT_LOCALE = 'fa';

// prettier-ignore
const appLocales = [
  'en',
  'fa',
  'tr',
  'ar'
];

const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages =
    locale !== DEFAULT_LOCALE
      ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
      : {};
  const flattenFormattedMessages = (formattedMessages, key) => {
    const formattedMessage =
      !messages[key] && locale !== DEFAULT_LOCALE
        ? defaultFormattedMessages[key]
        : messages[key];
    return Object.assign(formattedMessages, { [key]: formattedMessage });
  };
  return Object.keys(messages).reduce(flattenFormattedMessages, {});
};

const translationMessages = {
  en: formatTranslationMessages('en', enTranslationMessages),
  fa: formatTranslationMessages('fa', faTranslationMessages),
  tr: formatTranslationMessages('tr', trTranslationMessages),
  ar: formatTranslationMessages('ar', arTranslationMessages),
};

exports.appLocales = appLocales;
exports.formatTranslationMessages = formatTranslationMessages;
exports.translationMessages = translationMessages;
exports.DEFAULT_LOCALE = DEFAULT_LOCALE;
