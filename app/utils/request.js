import axios from 'axios';
const request = axios.create({
  // eslint-disable-next-line no-undef
  baseURL: config.url,
  timeout: 300000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});
request.interceptors.request.use(
  // eslint-disable-next-line func-names
  config => {
    // eslint-disable-next-line no-param-reassign
    config.headers['X-localization'] = localStorage.getItem('language') || 'fa';
    return config;
  },
  // eslint-disable-next-line func-names
  function(error) {
    return Promise.reject(error);
  },
);
export default request;
